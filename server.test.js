const request = require('supertest');
const app = require('./server'); // Server file name is server.js


describe('GET /issues', () => {
    test('should return all issues', async () => {
      const response = await request(app).get('/issues');
      expect(response.status).toBe(200);
      expect(response.body).toHaveLength(3); // For 3 hardcoded issues
    });
  });

  describe('POST /issues', () => {
    test('should create a new issue', async () => {
      const newIssue = { id: 4, title: 'New Issue', description: 'Description for New Issue' };
  
      const response = await request(app)
        .post('/issues')
        .send(newIssue);
  
      expect(response.status).toBe(201);
      expect(response.body).toEqual(newIssue);
    });
  });
  
  describe('PUT /issues/:id', () => {
    test('should update an existing issue', async () => {
      const updatedIssue = { title: 'Updated Issue', description: 'Updated Description' };
  
      const response = await request(app)
        .put('/issues/1') // Assuming issue with ID 1 exists
        .send(updatedIssue);
  
      expect(response.status).toBe(200);
      expect(response.body.title).toBe(updatedIssue.title);
      expect(response.body.description).toBe(updatedIssue.description);
    });
  });
  
  describe('DELETE /issues/:id', () => {
    test('should delete an existing issue', async () => {
      const response = await request(app)
        .delete('/issues/2'); // Assuming issue with ID 2 exists
  
      expect(response.status).toBe(200);
      expect(response.body.id).toBe(2);
      // Assert any other properties you want to verify about the deleted issue
    });
  });
  