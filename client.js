const axios = require('axios');
const readline = require('readline');

const rl = readline.createInterface({
  input: process.stdin,
  output: process.stdout
});

const BASE_URL = 'http://localhost:3000/issues';

// Function to send a POST request to create a new issue
async function createIssue() {
  rl.question('Enter issue title: ', async function(title) {
    rl.question('Enter issue description: ', async function(description) {
      rl.question('Enter issue status (open/closed): ', async function(status) {
        const issue = { title, description, status };
        try {
          const response = await axios.post(BASE_URL, issue);
          const createdIssue = response.data;
          console.log('Created issue:', createdIssue);
        } catch (error) {
          console.error('Error:', error.message);
        }
        rl.close();
      });
    });
  });
}

// Function to send a GET request to fetch all issues
async function getIssues() {
  rl.question('Enter issue status to filter (open/closed): ', async function(status) {
    try {
      const response = await axios.get(BASE_URL, { params: { status } });
      const filteredIssues = response.data;
      console.log('Filtered issues:', filteredIssues);
    } catch (error) {
      console.error('Error:', error.message);
    }
    rl.close();
  });
}

// Function to send a PUT request to update an existing issue
async function updateIssue() {
  rl.question('Enter issue ID to update: ', async function(id) {
    rl.question('Enter updated title: ', async function(title) {
      rl.question('Enter updated description: ', async function(description) {
        rl.question('Enter updated status (open/closed): ', async function(status) {
          const issue = { title, description, status };
          try {
            const response = await axios.put(`${BASE_URL}/${id}`, issue);
            const updatedIssue = response.data;
            console.log('Updated issue:', updatedIssue);
          } catch (error) {
            console.error('Error:', error.message);
          }
          rl.close();
        });
      });
    });
  });
}

// Function to send a DELETE request to delete an issue
async function deleteIssue() {
  rl.question('Enter issue ID to delete: ', async function(id) {
    try {
      const response = await axios.delete(`${BASE_URL}/${id}`);
      const deletedIssue = response.data;
      console.log('Deleted issue:', deletedIssue);
    } catch (error) {
      console.error('Error:', error.message);
    }
    rl.close();
  });
}

// CLI prompt
rl.question('Select an operation: (1) Create, (2) Read, (3) Update, (4) Delete: ', function(option) {
  if (option === '1') {
    createIssue();
  } else if (option === '2') {
    getIssues();
  } else if (option === '3') {
    updateIssue();
  } else if (option === '4') {
    deleteIssue();
  } else {
    console.log('Invalid option');
    rl.close();
  }
});

