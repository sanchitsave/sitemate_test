You can run the project by opening 2 terminals.
In terminal 1, type node server.js
In terminal 2, type node client.js
As per the requirement, when the client runs, CRUD options are given in CLI to play around with.

Unit Testing (Bonus requirement)
In your terminal, make sure port 3000 is not running by server first.
run npm test
The test ensures all CRUD opertations work under unit testing.


Filtered Issues (Bonus Requirement)
When the client side is running, and you press 2 to read all issues, you are asked to type open/close. 
This will only show open/close requests.
If you leave it blank and press enter, all issues will be shown