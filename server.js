const express = require('express');
const app = express();

// Parse JSON 
app.use(express.json());

// Data set for 3 issues, this is static for now
let issues = [
  { id: 1, title: 'Issue 1', description: 'Laptop Not working', status: 'open' },
  { id: 2, title: 'Issue 2', description: 'Mouse not working', status: 'closed' },
  { id: 3, title: 'Issue 3', description: 'Mic not working', status: 'open' }
];

let nextId = 4; // Initial ID for new issues

// GET route to fetch all issues for list
app.get('/issues', (req, res) => {
  const { status } = req.query;
  let filteredIssues = issues;
  if (status) {
    filteredIssues = issues.filter(issue => issue.status === status);
  }
  res.json(filteredIssues);
});

// POST route to create a new issue
app.post('/issues', (req, res) => {
  const { title, description, status } = req.body;
  const newIssue = { id: nextId, title, description, status };
  issues.push(newIssue);
  nextId++;
  res.status(201).json(newIssue);
});

// PUT route to update an issue
app.put('/issues/:id', (req, res) => {
  const { id } = req.params;
  const { title, description, status } = req.body;
  const issue = issues.find(issue => issue.id == id);
  if (!issue) {
    return res.status(404).json({ error: 'Issue not found' });
  }
  issue.title = title;
  issue.description = description;
  issue.status = status;
  res.json(issue);
});

// DELETE route to delete an issue
app.delete('/issues/:id', (req, res) => {
  const { id } = req.params;
  const index = issues.findIndex(issue => issue.id == id);
  if (index === -1) {
    return res.status(404).json({ error: 'Issue not found' });
  }
  const deletedIssue = issues.splice(index, 1);
  res.json(deletedIssue[0]);
});

// Start the server
const server = app.listen(3000, () => {
  console.log('Server started on port 3000');
});

module.exports = server; // Export the server object
